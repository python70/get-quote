#! /usr/bin/env python3

import requests
from bs4 import BeautifulSoup
import time
import sys
from PyQt5.QtWidgets import *#(QLabel, QRadioButton, QPushButton, QVBoxLayout, QApplication, QWidget, QLineEdit)

def fetch_quote(SYB, save): # function to pull quotes
    """fetch_quote goes to finance.yahoo.com and pulls & prints the requested stock information."""
    page = requests.get('https://finance.yahoo.com/quote/' + SYB +'/key-statistics?p='+ SYB)
    time.sleep(1)
    home_page = requests.get('https://finance.yahoo.com/quote/' + SYB + '?p=' + SYB)

    soup = BeautifulSoup(page.text, 'html.parser')
    soup1 = BeautifulSoup(home_page.text, 'html.parser')
    last_links = soup.find(class_='W(100%) Whs(nw) Ovx(a)')
    
    try: # this handles the event of a symbol not being found
        last_links.decompose()
    except AttributeError:
        print("Ticker Symbol Does Not Exist!")
    else:
        divs_and_splits = soup.find_all(class_='W(100%) Bdcl(c)')[2]
        dividend_payout = divs_and_splits.find_all(class_='Bxz(bb) H(36px) BdB Bdbc($seperatorColor)')[4]
        payout_percent = dividend_payout.find(class_='Fw(500) Ta(end) Pstart(10px) Miw(60px)')
        payout_ratio = dividend_payout.find('span').get_text()

        div_per_share = soup.find_all(class_='Bxz(bb) H(36px) BdY Bdc($seperatorColor)')[2]
        annual_div = div_per_share.find(class_='Fw(500) Ta(end) Pstart(10px) Miw(60px)')
        forward_annual_dividend_rate = div_per_share.find('span').get_text()

        div_yield = divs_and_splits.find_all(class_='Bxz(bb) H(36px) BdB Bdbc($seperatorColor)')[0]
        div_percent = div_yield.find(class_='Fw(500) Ta(end) Pstart(10px) Miw(60px)')
        forward_annual_dividend_yield = div_yield.find('span').get_text()
        
        price_quote = soup1.find(class_='My(6px) Pos(r) smartphone_Mt(6px)')
        price = price_quote.find('span').get_text()
    # Uncomment any print lines if you want -- the are left there for testing purposes    
        #print("\n")
        #print(SYB)
        #print(price)

        for payout in payout_percent:
            try:
                data = payout
                #print(payout_ratio + ' ' + data)
            except TypeError:
                payout = "Payout ratio not found!"
                #print(payout)

        for rate in annual_div:
            try:
                data = rate
                rate = ("$ " + data)
                #print(forward_annual_dividend_rate + ' $' + data)
            except TypeError:
                 rate = "Forward dividend rate not found!"
                 #print(rate)

        for yield_ in div_percent:
            try:
                data = yield_
                #print(forward_annual_dividend_yield + ' ' + data)
            except TypeError:
                yield_ = "Forward annual dividend yield not found!"
                #print(yield_)

        output = (SYB, price, payout_ratio, payout, forward_annual_dividend_rate, rate, forward_annual_dividend_yield, yield_)
         
        if save == 1:
            save_info(output)
        return SYB, price, payout_ratio, payout, forward_annual_dividend_rate, rate, forward_annual_dividend_yield, yield_

def save_info(output): #saves to data file
    """save_info saves the output from fetch_quote when the whole list is run"""
    with open("quotes.dat", "a") as quotes:
        quotes.write(output[0]+ "\n")
        quotes.write("Price: $" + str(output[1])+ "\n")
        quotes.write(str(output[2])+ ": " + str(output[3])+ "\n")
        quotes.write(str(output[4])+ ": " + str(output[5])+ "\n")
        quotes.write(str(output[6])+ ": " + str(output[7])+ "\n")
        quotes.write("\n")
    quotes.close()

def get_one():   #function to pull 1 and loop back to program
    """get_one calls fetch_quote and pulls just one quote and does not store it"""
    
    save = 0 
    SYB = input("Enter symbol to get quote: ")
    fetch_quote(SYB, save)

def walk_list():    #function to pull list
    """walk_list loops through and pulls quotes on the entire list you provide it from Stocks.dat"""
    with open("Stocks.dat", "r") as Holdings:
        lines = [line.rstrip() for line in Holdings]

        for line in lines:

            save = 1 
            SYB = line
            fetch_quote(SYB, save)
    Holdings.close()
    #print("Outputting data to 'quotes.dat...'")

class MainWindow(QWidget):

    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        
        self.run = QPushButton("Run", self)
        self.run.setToolTip("Press to fetch quote(s)")
        self.run.clicked.connect(self.runButton)
        self.exit = QPushButton("Exit", self)
        self.exit.setToolTip("Press to exit application")
        self.exit.clicked.connect(self.close)

        self.textbox = QLineEdit(self)
        self.textbox.move(20, 20)
        self.textbox.resize(280,40)
       
        self.label = QLabel('Select an option below:')
        self.rbtn1 = QRadioButton('Enter Ticker Symbol for Single Quote')
        self.rbtn2 = QRadioButton('Run Whole Batch')
        self.label2 = QLabel("")
        self.label3 = QLabel("")
        
        self.rbtn1.toggled.connect(self.onClicked)
        self.rbtn2.toggled.connect(self.onClicked)
        

        layout = QVBoxLayout()
        layout.addWidget(self.textbox)
        layout.addWidget(self.label)
        layout.addWidget(self.rbtn1)
        layout.addWidget(self.rbtn2)
        layout.addWidget(self.label2)
        layout.addWidget(self.label3)
        layout.addWidget(self.run)
        layout.addWidget(self.exit)
     
        self.setGeometry(300, 300, 1000, 500)

        self.setLayout(layout)
        self.setWindowTitle('Get Quotes!')

        

        self.show()

    def onClicked(self):
        radioBtn = self.sender()
        
        if radioBtn.isChecked():
            self.label2.setText("You opted to: " + radioBtn.text())
        return radioBtn.text() 
    
    def runButton(self):
        button = self.sender()
        option = self.label2.text()
        opt1 = 'You opted to: Enter Ticker Symbol for Single Quote'
        opt2 = 'You opted to: Run Whole Batch'
        if option == opt1:
            save = 0
            SYB = self.textbox.text()
            self.label3.repaint()
            try:    
                output = fetch_quote(SYB, save)
                self.label3.setText(SYB + "\n" "Price: $" + str(output[1])+ "\n" 
                                    + str(output[2])+ ": " + str(output[3])+ "\n"
                                    + str(output[4])+ ": " + str(output[5])+ "\n"
                                    + str(output[6])+ ": " + str(output[7])+ "\n")
            except TypeError:
                self.label3.setText("Ticker Symbol Does Not Exist!")
        elif option == opt2:
            save = 1
            walk_list()
            self.label3.setText("All Done! Open quotes.dat to see list.")

        else:
            self.label3.setText("Please select an option before clicking run.")

if __name__ == '__main__':    
    app = QApplication(sys.argv)
    ex = MainWindow()
    sys.exit(app.exec_()) 
